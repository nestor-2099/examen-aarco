/*
 * Examen Aarco 2021
 * Néstor Guadalupe García Ortega
 */

(function(){
	var slideController = function() {
		var menuHome = $(".menu-home"),
		menuAbout = $(".menu-about"),
		menuContact = $(".menu-contact"),
		page1 = $(".page-1"),
		page2 = $(".page-2"),
		contactForm = $(".contact-form"),
		menuOpt = $(".menu-opt");

		menuHome.click(function(){
			menuOpt.removeClass("active");
			$(this).addClass("active");
			page1.show()
			page2.hide()
		});

		menuAbout.click(function(){
			menuOpt.removeClass("active");
			$(this).addClass("active");
			page1.hide()
			page2.show()
		});

		menuContact.click(function(){
			contactForm.addClass("active")
		});

		contactForm.click(function(){
			contactForm.removeClass("active")
		}).children().click(function(e) {
			return false;
		});
	}

	var menuController = function() {
		var menuBtn = $(".menu-responsive"),
		links = $("#links");

		menuBtn.click(function(){
			links.toggle()
		});
	}

	var init = function() {
		slideController();
		menuController();
	}

	init();
}) ();